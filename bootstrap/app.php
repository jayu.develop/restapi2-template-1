<?php

session_start();

require __DIR__ . '/../vendor/autoload.php';

/**
* 
*/

class Database
{
	private $pdo;
	
	public function __construct(PDO $pdo)
	{
		$this->pdo = $pdo;
	}

	public function query($sql)
	{
		return $this->pdo->prepare($sql);
	}
}

$app = new \Slim\App([
	'settings' => [
		'displayErrorDetails' => true,
	],
	
]);

$container = $app->getContainer();

$container['pdo'] = function () {

	$dbhost = "localhost";
	$dbusername = "root";
	$dbpassword = "";
	$dbname = "customer";

	$pdo = new PDO("mysql:host=". $dbhost .";dbname=". $dbname.";charset=utf8", $dbusername, $dbpassword);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $pdo;
};

$container['db'] = function ($container) {
	return new Database($container->pdo);
}; 

$container['pdo2'] = function () {

	$dbhost = "localhost";
	$dbusername = "root";
	$dbpassword = "";
	$dbname = "hongkhai";

	$pdo = new PDO("mysql:host=". $dbhost .";dbname=". $dbname.";charset=utf8", $dbusername, $dbpassword);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $pdo;
};

$container['db2'] = function ($container) {
	return new Database($container->pdo2);
}; 

$container['HomeController'] = function ($container) {
	return new \App\Controllers\HomeController($container);
};

$container['loginController'] = function ($container) {
	return new \App\Controllers\loginController($container);
};

$container['permController'] = function ($container) {
	return new \App\Controllers\permController($container);
};

$container['recomBookController'] = function ($container) {
	return new \App\Controllers\recomBookController($container);
};

$container['scReportController'] = function ($container) {
	return new \App\Controllers\scReportController($container);
};

$container['customerController'] = function ($container) {
	return new \App\Controllers\customerController($container);
};

$container['scheduleController'] = function ($container) {
	return new \App\Controllers\scheduleController($container);
};

$container['configController'] = function ($container) {
	return new \App\Controllers\configController($container);
};


require __DIR__ . '/../app/routes.php';