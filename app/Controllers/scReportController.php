<?php

namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface;
use \Psr\Http\Message\ResponseInterface;
use PDO;

/**
* 
*/
class scReportController extends Controller
{

	public function scReportToday(ServerRequestInterface $request, ResponseInterface $response)
	{
		$sqlControl = $this->container->db2->query("SELECT * FROM report_selfcheck WHERE DATE(datetime) = CURDATE()");
		$sqlControl->execute();
		$result = $sqlControl->fetchAll(PDO::FETCH_OBJ);	

		$userStatus = 'SUCCESS';
		$userMsg    = 'Show report Selfcheck.';	
		
		$status = 	[
						'status' => $userStatus,
						'data'   => $result,
						'msg'    => $userMsg
					];
		$response = $this->response->withJson($status);
		return $response;
	}
}