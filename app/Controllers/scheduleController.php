<?php

namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface;
use \Psr\Http\Message\ResponseInterface;
use PDO;

class scheduleController extends Controller{
    public function scheduleShow(ServerRequestInterface $request, ResponseInterface $response)
	{

        $sqlControl = $this->container->db2->query("SELECT 
        M_2.hardware_id, M_2.open_time, M_2.close_time, M_2.weekday, M_2.active 
        FROM customer.hardware AS M 
        JOIN hongkhai.schedule AS M_2 ON M.hardware_id = M_2.hardware_id 
        WHERE M.code = 'sc'");

        $sqlControl->execute();
        $showData = $sqlControl->fetchAll(PDO::FETCH_OBJ);

        $showStatus = 'SUCCESS';
        $recMsg    = '';

        $status = 	[
                        'status' => $showStatus,
                        'data'   => $showData,
                        'msg'    => $recMsg
                    ];

        $response = $this->response->withJson($status);
        return $response;
    }

    public function scheduleUpdateActive(ServerRequestInterface $request, ResponseInterface $response)
	{
        $id = $request->getAttribute('no');
        $active = $request->getParam('active');

        $sqlControl = $this->container->db2->query("UPDATE schedule SET active = :active 
        WHERE no = $id");

        $sqlControl->bindParam(':active', $active);
        $sqlControl->execute();
        $status = 	[
                        'status' => "SUCCESS",
                        'data'   => "",
                        'msg'    => "Upload Success etc."
                    ];

        $response = $this->response->withJson($status);
        return $response;
    }

    public function scheduleAdd(ServerRequestInterface $request, ResponseInterface $response)
	{
        $postData = $request->getParsedBody();
        $hardware_id = $postData['hardware_id'];
        $open_time = $postData['open_time'];
        $close_time = $postData['close_time'];
        $weekday = $postData['weekday'];
        $active = $postData['active'];

        $checktime = $this->container->db2->query("SELECT hardware_id AS OT, weekday AS WD FROM schedule 
        WHERE (open_time = '$open_time' OR close_time = '$close_time') 
        OR (open_time = '$close_time' OR close_time = '$open_time')");

        $checktime->execute();
        $checktime1 = $checktime->fetchAll(PDO::FETCH_OBJ);

        if($checktime1[0]->OT == 0) {
            $sqlControl = $this->container->db2->query("INSERT INTO 
            schedule(hardware_id, open_time, close_time, weekday, active) 
            VALUES(:hardware_id, :open_time, :close_time, :weekday, :active)");
            
            $sqlControl->bindParam(':hardware_id', $hardware_id);
            $sqlControl->bindParam(':open_time', $open_time);
            $sqlControl->bindParam(':close_time', $close_time);
            $sqlControl->bindParam(':weekday', $weekday);
            $sqlControl->bindParam(':active', $active);
            $sqlControl->execute();

            $status =   [
                            'status' => "SUCCESS",
                            'data'   => "",
                            'msg'    => "Upload Success etc."
                        ];
        }else{
            $status = 	[
                            'status' => "FAIL",
                            'data'   => "",
                            'msg'    => "Upload fail"
                        ];
        }

        $response = $this->response->withJson($status);
        return $response;
    }

    public function scheduleDelete(ServerRequestInterface $request, ResponseInterface $response)
	{
        $hardwareID = $request->getAttribute('hardwareID');
        
        $sqlControl = $this->container->db2->query("DELETE FROM schedule WHERE no = :id");
        $sqlControl->bindParam(':id', $hardwareID);
        $sqlControl->execute();
        
        $status = 	[
                        'status' => "SUCCESS",
                        'data'   => "",
                        'msg'    => "Record deleted successfully"
                    ];
        $response = $this->response->withJson($status);
        //$response = $this->response->withJson($deleteBook);
        return $response;
    }
}