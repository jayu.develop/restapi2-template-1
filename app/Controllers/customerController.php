<?php

namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface;
use \Psr\Http\Message\ResponseInterface;
use PDO;

class customerController extends Controller{
    public function customerAdd(ServerRequestInterface $request, ResponseInterface $response)
	{
        $postData = $request->getParsedBody();
        $name = $postData['name'];
        $position = $postData['position'];
        $email = $postData['email'];
        $username = $postData['username'];
        $password = $postData['password'];
        $control_list = $postData['control_list'];
        $report_list = $postData['report_list'];

        $sqlControl = $this->container->db->query("INSERT INTO staff(name, position, email, username, password, type) 
        VALUES(:name, :position, :email, :username, :password, 3);
        INSERT INTO staff_permission(control_list, report_list) 
        VALUES(:control_list, :report_list);");

        $sqlControl->bindParam(':name', $name);
        $sqlControl->bindParam(':position', $position);
        $sqlControl->bindParam(':email', $email);
        $sqlControl->bindParam(':username', $username);
        $sqlControl->bindParam(':password', $password);
        $sqlControl->bindParam(':control_list', $control_list);
        $sqlControl->bindParam(':report_list', $report_list);
        $sqlControl->execute();
        
        $status = 	[
                        'status' => "SUCCESS",
                        'data'   => "",
                        'msg'    => "Upload Success etc."
                    ];

        $response = $this->response->withJson($status);
        return $response;
    }

    public function usernameDuplicate(ServerRequestInterface $request, ResponseInterface $response)
    {
        $postData = $request->getParsedBody();
        $username = $postData['username'];

        $checkUsername = $this->container->db->query("SELECT staff_id AS SI FROM staff 
        WHERE username = '$username'");

        $checkUsername->execute();
        $checkUsername1 = $checkUsername->fetchAll(PDO::FETCH_OBJ);

        if($checkUsername1[0]->SI == 0) {

            $status =   [
                            'status' => "SUCCESS",
                            'data'   => "",
                            'msg'    => "Username Success etc."
                        ];
        }else{
            $status = 	[
                            'status' => "FAIL",
                            'data'   => "",
                            'msg'    => "Username Duplicate"
                        ];
        }

        $response = $this->response->withJson($status);
        return $response;
    }

    public function customerUpdate(ServerRequestInterface $request, ResponseInterface $response)
    {
        $staff_id = $request->getAttribute('staff_id');
        $name = $request->getParam('name');
        $position = $request->getParam('position');
        $email = $request->getParam('email');
        $username = $request->getParam('username');
        $password = $request->getParam('password');
        $type = $request->getParam('type');
        $control_list = $request->getParam('control_list');
        $report_list = $request->getParam('report_list');

        $sqlControl = $this->container->db->query("UPDATE staff JOIN staff_permission 
        ON staff.staff_id = staff_permission.staff_id SET 
        staff.name = :name, 
        staff.position = :position, 
        staff.email = :email, 
        staff.username = :username, 
        staff.password = :password, 
        staff.type = :type, 
        staff_permission.control_list = :control_list,  
        staff_permission.report_list = :report_list 
        WHERE staff.staff_id = $staff_id");

        $sqlControl->bindParam(':name', $name);
        $sqlControl->bindParam(':position', $position);
        $sqlControl->bindParam(':email', $email);
        $sqlControl->bindParam(':username', $username);
        $sqlControl->bindParam(':password', $password);
        $sqlControl->bindParam(':type', $type);
        $sqlControl->bindParam(':control_list', $control_list);
        $sqlControl->bindParam(':report_list', $report_list);
        $sqlControl->execute();
        $status = 	[
                        'status' => "SUCCESS",
                        'data'   => "",
                        'msg'    => "Upload Success etc."
                    ];

        $response = $this->response->withJson($status);
        return $response;
    }

    public function customerLockandUnlock(ServerRequestInterface $request, ResponseInterface $response)
    {
        $staff_id = $request->getAttribute('staff_id');
        $status = $request->getParam('status');

        $sqlControl = $this->container->db->query("UPDATE staff SET status = :status 
        WHERE staff_id = $staff_id");

        $sqlControl->bindParam(':status', $status);
        $sqlControl->execute();
        $status = 	[
                        'status' => "SUCCESS",
                        'data'   => "",
                        'msg'    => "Upload Success etc."
                    ];

        $response = $this->response->withJson($status);
        return $response;
    }

    public function customerShowAll(ServerRequestInterface $request, ResponseInterface $response)
    {
        $sqlControl = $this->container->db->query("SELECT 
            staff.staff_id    as id, 
            staff.name        as name, 
            staff.username    as username, 
            staff.email       as email, 
            staff_permission.control_list as control_list, 
            staff_permission.report_list as report_list 
            
            FROM staff INNER JOIN staff_permission ON staff.staff_id = staff_permission.staff_id");

		$sqlControl->execute();
		$showData = $sqlControl->fetchAll(PDO::FETCH_OBJ);

		$showStatus = 'SUCCESS';
		$recMsg    = '';

		$status = 	[
						'status' => $showStatus,
						'data'   => $showData,
						'msg'    => $recMsg
					];

		$response = $this->response->withJson($status);
		return $response;
    }

    public function customerShowsingle(ServerRequestInterface $request, ResponseInterface $response)
    {
        $staff_id = $request->getAttribute('staff_id');
        $sqlControl = $this->container->db->query("SELECT 
            staff.staff_id    as id, 
            staff.name        as name, 
            staff.username    as username, 
            staff.email       as email, 
            staff_permission.control_list as control_list, 
            staff_permission.report_list as report_list 
            FROM staff INNER JOIN staff_permission ON staff.staff_id = staff_permission.staff_id 
            WHERE staff.staff_id = $staff_id");

		$sqlControl->execute();
		$showData = $sqlControl->fetchAll(PDO::FETCH_OBJ);

		$showStatus = 'SUCCESS';
		$recMsg    = '';

		$status = 	[
						'status' => $showStatus,
						'data'   => $showData,
						'msg'    => $recMsg
					];

		$response = $this->response->withJson($status);
		return $response;
    }
}