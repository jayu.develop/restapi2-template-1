<?php

namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface;
use \Psr\Http\Message\ResponseInterface;
use PDO;

class configController extends Controller{
    public function configUpdate(ServerRequestInterface $request, ResponseInterface $response)
    {
        $type = $request->getAttribute('type');
        $name = $request->getAttribute('name');
        $config_value = $request->getParam('config_value');

        $sqlControl = $this->container->db2->query("UPDATE config SET 
        config_value = :config_value  
        WHERE type = '$type' AND name = '$name'");

        $sqlControl->bindParam(':config_value', $config_value);
        $sqlControl->execute();
        $status = 	[
                        'status' => "SUCCESS",
                        'data'   => "",
                        'msg'    => "Upload Success etc."
                    ];

        $response = $this->response->withJson($status);
        return $response;
    }

    public function configShowSC(ServerRequestInterface $request, ResponseInterface $response)
    {
        $sqlControl = $this->container->db2->query("SELECT 
            name as name, config_value as config_value 
            FROM config WHERE type = 'sc'");

		$sqlControl->execute();
		$showData = $sqlControl->fetchAll(PDO::FETCH_OBJ);

		$showStatus = 'SUCCESS';
		$recMsg    = '';

		$status = 	[
						'status' => $showStatus,
						'data'   => $showData,
						'msg'    => $recMsg
					];

		$response = $this->response->withJson($status);
		return $response;
    }
}