<?php

$app->get('/', 'HomeController:indexQuery');
$app->get('/{name}', 'HomeController:indexGet');

$app->post('/checkLogin/check', 'loginController:checkLogin');

$app->get('/permControl/{userID}', 'permController:permControl');
$app->get('/permReport/{userID}', 'permController:permReport');

$app->get('/recommendedBook/show', 'recomBookController:recomBook');
$app->post('/recommendedBook/add', 'recomBookController:recomBookAdd');
$app->delete('/recommendedBook/delete/{bookID}', 'recomBookController:recomBookDelete');

$app->get('/scReport/today', 'scReportController:scReportToday');

$app->post('/customerController/add', 'customerController:customerAdd');
$app->post('/customerController/usernameDuplicate', 'customerController:usernameDuplicate');
$app->put('/customerController/update/{staff_id}', 'customerController:customerUpdate');
$app->put('/customerController/lock/{staff_id}', 'customerController:customerLockandUnlock');
$app->get('/customerController/showall', 'customerController:customerShowAll');
$app->get('/customerController/showsingle/{staff_id}', 'customerController:customerShowsingle');

$app->get('/scheduleController/show', 'scheduleController:scheduleShow');
$app->put('/scheduleController/updateActive/{no}', 'scheduleController:scheduleUpdateActive');
$app->post('/scheduleController/add', 'scheduleController:scheduleAdd');
$app->delete('/scheduleController/delete/{hardwareID}', 'scheduleController:scheduleDelete');

$app->put('/configController/update/{type}/{name}', 'configController:configUpdate');
$app->get('/configController/showSC', 'configController:configShowSC');
